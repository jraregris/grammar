defmodule GrammarTest do
  use ExUnit.Case
  doctest Grammar

  test "generate with empty grammar returns symbol" do
    empty_grammar = Grammar.empty()
    assert Grammar.generate(empty_grammar, "S") == "S"
  end

  test "direct grammar returns first hit" do
    result = %{"N" => "ball"} |> Grammar.generate("N")
    assert result == "ball"
  end

  test "recursive grammar recurses" do
    result =
      %{"NP" => "N", "N" => "ball"}
      |> Grammar.generate("NP")

    assert result == "ball"
  end

  test "branching grammar branches" do
    grammar = %{"S" => [["V", "N"]], "N" => "ball", "V" => "kick"}
    assert Grammar.generate(grammar, "S") == "kick ball"
  end

  test "several options" do
    grammar = %{"N" => ["ball", "foot"]}
    assert Grammar.generate(grammar, "N") in ["ball", "foot"]
  end

  test "the example grammar" do
    %{
      "S" => [~w(NP VP), ~w(S and S)],
      "NP" => [~w(Art N), ~w(Name)],
      "VP" => [~w(V NP)],
      "Art" => ~w(the a every some),
      "N" => ~w(man ball woman table dog cat wombat),
      "V" => ~w(hit took saw liked worshiped remembered),
      "Name" => ~w(Alice Bob Carlos Dan Eve)
    }
    |> Grammar.generate("S")
    |> IO.puts()
  end
end
