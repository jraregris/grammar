defmodule Grammar do
  def empty() do
    %{}
  end

  def generate(grammar, symbols) when is_list(symbols) do
    symbol = Enum.random(symbols)

    if is_list(symbol) do
      symbol |> Enum.map(&generate(grammar, &1)) |> Enum.join(" ")
    else
      generate(grammar, symbol)
    end
  end

  def generate(grammar, symbol) do
    case Map.get(grammar, symbol) do
      nil -> symbol
      something -> generate(grammar, something)
    end
  end
end
